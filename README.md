Tic-Tac-Toe README
=================================================
This repo is a development repo for my Tic Tac Toe C++ application. 

This project is a programming challenge assigned by the Flatiron School. 

For my project, I opted to use some of the C++ skills I've been sharpening
this semester and test my skills. This was a very enjoyable project. Since 
I haven't done any GUI development aside from basic web development, I 
opted to try and make a strong case for my backend skills. 

For the assignment, I opted to build my game board by using a character 
array. I used a 9 element array to initialize with +1 of the appropriate 
space (grid[0] displays '1', grid[1] displays 2, and so forth). I decided 
to use a character array and to display it through a for loop that displays 
the table across 3 lines to form 9 boxes using a for loop and instead of 
incrementing the counter/index by one after each run, I opted to display 
3 elements, and iterate by 3. This worked nicely and with a little 
formatting ended up being a bit easier to deal with than a 2D array. 

I built a total of 4 additional functions which main will call. I used a 
void  function to display the board, whose parameters are a pointer to 
the array and the const int array size to allow the values of the array 
to be displayed and manipulated without explicitly returning any value to
main. Additionally, I used void functions to grad player 1 and player 2 
input, again parameters being the char array and the array size. I used 
a bool function determine is there was a winner or a tie, by creating a 
flag and setting it to false. The function will return false unless all 
possible combinations of X or O winning or tying are drawn. In that case 
it will return true, and a global string variable "winner" will display 
if P1 or P2 won, or if it's a tie. 
