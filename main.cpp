/*
 Author: Brandon Johnson
 Description: Tic Tac Toe Project
 */

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <string>
using namespace std;

//********************Function Declarations*********************
void *showBoard(char*, const int);	// Displays the gameboard
void getP1Input(char*, const int);	// Record user 1 selection
void getP2Input(char*, const int);	// Record user 2 selection
bool checkIfWin(char*);				// Checks winning combinations
//***********************End Declarations************************


// Global Variables
string player1;                 // Player 1's Name
string player2;                 // Player 2's Name
string winner;                  // To hold the winner's name
string draw = "Nobody";         // String for name if tie occurs


int main()
{
	// Local Variables
	const int SIZE = 9;			// Size of the array
	char choices[SIZE] = {'1','2','3','4','5','6','7','8','9'};
	char yesNo;
    
    
	// Welcome message
	system("clear");
	cout << "Welcome to T3! Old school, Command Line Tic-Tac-Toe!\n"
    "Ready to play? (Press Y to play, any other key to exit)\n\n";
	
	cin >> yesNo;
	cin.ignore();
	
	if ((yesNo == 'Y') || (yesNo == 'y'))
	{
        system("clear");
        
		cout << "***************************************************\n";
        cout << "*** T3: Tic-Tac-Toe Console Game by Interjector ***\n";
        cout << "***************************************************\n";

        cout << "Player 1, please enter your name: ";		// Get player 1's name
		getline(cin,player1);
		cout << "Player 2, please enter your name: ";		// Get player 2's name
		getline(cin,player2);
		cout << "Hello " << player1 << ". You will be X. " << endl;
		cout << "Today you will be facing " << player2 << ". Who will be O."<< endl << endl;
		
		// Use while loop to run through array and
        // check for winners after each player has made move
		showBoard(choices, SIZE);   // Show the board
		// Loop through gameplay
		do
		{
                getP1Input(choices, SIZE);
				showBoard(choices, SIZE);
				if((checkIfWin(choices) == false))
                {
                    getP2Input(choices, SIZE);
                    showBoard(choices, SIZE);
                }
		}while((checkIfWin(choices) == false));
        if ((winner == player1) || (winner == player2))
            cout << winner << " wins!\n";
        else
            cout << "It's a draw!\n";

        
	}	// End if statement
    
	return 0;
}//******************END MAIN**********************!


//***********BEGIN FUNCTION DEFINITIONS**************
//***************************************************
// The showBoard function will display the array of *
// characters broken across 3 lines to make up the  *
// Tic Tac Toe grid. When the function is called it *
// will display the contents after each players has *
// taken a turn. It's a void, nothing to return main*
//***************************************************
void *showBoard(char* grid, const int elem)
{
	int size = elem-1;
    system("clear");
    cout << "***************************************************\n";
    cout << "*** T3: Tic-Tac-Toe Console Game by Interjector ***\n";
    cout << "***************************************************\n\n";
    cout << setw(15) << player1 << setw(3) << ": X" << setw(15) << player2 << setw(3) << ": O\n\n";
	for (int i=0; i<elem; i+=3)
	{
		cout << "\t\t   " << grid[i] << " | " << grid[i+1] << " | " << grid[i+2] << endl;
		if ((i+2) < size)
			cout << "\t\t   ---------" << endl;
	}
    return 0;
}//******************END FUNCTION********************!


//***************************************************
// The getP1Input function take an array pointer and*
// const array SIZE variables arguements as params. *
// The function will ask the user to input a choice *
// selection. A while loop will validate that the   *
// users choice is not already taken. If it passes  *
// the data validation, it will store the user's    *
// input as an 'X' mark to the corresponding spot in*
// the array -1 (arr[0] == space #1). This is a void*
// function and does not need a return since we used*
// an array pointer rather than passing the array in*
//***************************************************
void getP1Input(char* grid, const int elem)
{
	int input;
	cout << endl << player1 << ", please make a selection: ";
	cin >> input;
   
    // Validate that space isn't taken
    while((grid[input-1] == 'X') || (grid[input-1] == 'O'))
    {
        cout << "That space is already taken!\n";
        cout << "Please enter another selection: ";
        cin >> input;
    }
	grid[input-1] = 'X';
}//*******************END FUNCTION********************!


//************BEGIN FUNCTION DEFINITION**************
// The getP2Input function take an array pointer and*
// const array SIZE variables arguements as params. *
// The function will ask the user to input a choice *
// selection. A while loop will validate that the   *
// users choice is not already taken. If it passes  *
// the data validation, it will store the user's    *
// input as an 'O' mark to the corresponding spot in*
// the array -1 (arr[0] == space #1). This is a void*
// function and does not need a return since we used*
// an array pointer rather than passing the array in*
//***************************************************
void getP2Input(char* grid, const int elem)
{
	int input;
	cout << endl << player2 << ", please make a selection: ";
	cin >> input;
    // Validate that space isn't taken
    while((grid[input-1] == 'X') || (grid[input-1] == 'O'))
    {
        cout << "That space is already taken!\n";
        cout << "Please enter another selection: ";
        cin >> input;
    }
	grid[input-1] = 'O';
}//*******************END FUNCTION********************!

//***************************************************
// The checkIfWin function takes the array pointer  *
// arg as it's parameter. It checks all possible    *
// winning combinations. If a combination occurs it *
// will return the isWinner bool flag variable as   *
// true to main. If all values are filled, and there*

//***************************************************
bool checkIfWin(char* grid)
{
	// If statements to test all conditions that someone wins
	bool isWinner = false;
    
	if ((grid[0] == 'X') && (grid[1] == 'X') && (grid[2] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
    
	else if ((grid[0] == 'O') && (grid[1] == 'O') && (grid[2] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
	else if ((grid[3] == 'X') && (grid[4] == 'X') && (grid[5] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
	else if ((grid[3] == 'O') && (grid[4] == 'O') && (grid[5] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
	else if ((grid[6] == 'X') && (grid[7] == 'X') && (grid[8] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
	else if ((grid[6] == 'O') && (grid[7] == 'O') && (grid[8] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
	else if ((grid[0] == 'X') && (grid[3] == 'X') && (grid[6] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
	else if ((grid[0] == 'O') && (grid[3] == 'O') && (grid[6] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
	else if ((grid[1] == 'X') && (grid[4] == 'X') && (grid[7] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
	else if ((grid[1] == 'O') && (grid[4] == 'O') && (grid[7] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
	else if ((grid[2] == 'X') && (grid[5] == 'X') && (grid[8] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
	else if ((grid[2] == 'O') && (grid[5] == 'O') && (grid[8] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
	else if ((grid[0] == 'X') && (grid[4] == 'X') && (grid[8] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
	else if ((grid[0] == 'O') && (grid[4] == 'O') && (grid[8] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
	else if ((grid[2] == 'X') && (grid[4] == 'X') && (grid[6] == 'X'))
	{
		isWinner = true;
		winner = player1;
	}
	else if ((grid[2] == 'O') && (grid[4] == 'O') && (grid[6] == 'O'))
	{
		isWinner = true;
		winner = player2;
	}
    else if(((grid[0]=='X')||(grid[0]=='O')) &&
            ((grid[1]=='X')||(grid[1]=='O')) &&
            ((grid[2]=='X')||(grid[2]=='O')) &&
            ((grid[3]=='X')||(grid[3]=='O')) &&
            ((grid[4]=='X')||(grid[4]=='O')) &&
            ((grid[5]=='X')||(grid[5]=='O')) &&
            ((grid[6]=='X')||(grid[6]=='O')) &&
            ((grid[7]=='X')||(grid[7]=='O')) &&
            ((grid[8]=='X')||(grid[8]=='O')))
    {
        isWinner = true;
        winner = draw;
    }
	else isWinner = false;
    
	return isWinner;
}//*******************END FUNCTION********************!

